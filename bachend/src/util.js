import { CARDS } from './cards';

export function random(min, max) {
    return Math.round((Math.random() * max) + min);
}

export function getPlayerCards() {
    const player1 = [];
    const player2 = [];

    let cardObj = Object.assign({}, CARDS);
    let cardList = Object.keys(cardObj);
    const numberOfLoops = cardList.length / 2 - (cardList.length / 2 === 1 ? 1 : 0);

    for (let index = 0; index <= numberOfLoops; index++) {
        let player1CardKey = null;
        let player2CardKey = null;

        while (player1CardKey === player2CardKey && index !== numberOfLoops) {
            player1CardKey = cardList[random(0, cardList.length-1)];
            player2CardKey = cardList[random(0, cardList.length-1)];
        }

        player1.push(player1CardKey);
        player2.push(player2CardKey);
        delete cardObj[player1CardKey];
        delete cardObj[player2CardKey];

        cardList = Object.keys(cardObj);
        cardList = cardList.filter(value => !!value);
    }

    return [player1.filter(value => !!value), player2.filter(value => !!value)];
}

/*
 * Return -1 for match
 * Return 0 for player one
 * Return 1 for player two
 */
export function checkMatchWinner(key, player1Card, player2Card) {
    if (!player1Card[key] || !player2Card[key] || player1Card[key] === player2Card[key]) {
        return -1;
    }

    switch (key) {
        // Wich value needs bigger
        case 'thc':
        case 'sativa':
        case 'indika':
        case 'height':
        case 'harvestRate':
            return player1Card[key] > player2Card[key] ? 0 : 1;
        // Wich value need smaller
        case 'harvestTime':
            return player1Card[key] < player2Card[key] ? 0 : 1;
    }
}

export function changeCardOrder(cards) {
    const topCard = cards[0];
    const newCards = cards.slice(1, cards.length);
    newCards.push(topCard);
    return newCards;
}

export function pushWinner(winnerCards, loserCards) {
    console.log('loserCards[0]', loserCards[0]);
    winnerCards.push(winnerCards[0]);
    winnerCards = winnerCards.slice(1, winnerCards.length);
    winnerCards.push(loserCards[0]);
    loserCards = loserCards.slice(1, loserCards.length);
    return [winnerCards.filter(card => !!card), loserCards.filter(card => !!card)];
}


/*
let isRunning = true;
let loops = 0;
let [player1, player2] = getPlayerCards();
while (isRunning) {
  // console.log('loop', loops++, player1.length, player1.length, player2.length, player1[0], player2[0]);
  const winner = checkMatchWinner('thc', CARDS[player1[0]], CARDS[player2[0]]);

  if (winner === -1) {
    player1 = changeCardOrder(player1);
    player2 = changeCardOrder(player2);
    continue;
  }


  if (winner === 1) {
    [player1, player2] = pushWinner(player1, player2)
  } else {
    [player2, player1] = pushWinner(player2, player1)
  }

  if (player1.length === 0) {
    isRunning = false;
    console.log('Player 2 have won');
  }

  if (player2.length === 0) {
    isRunning = false;
    console.log('Player 1 have won');
  }
}
*/