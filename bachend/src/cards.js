export const CARDS = {
    "amnesiaHaze": {
        "name": "Amnesia Haze",
        "thc": 22,
        "sativa": 70,
        "indika": 30,
        "harvestRate": 700,
        "height": 210,
        "harvestTime": 11
    },
    "ultraSkunk": {
        "name": "Ultra Skunk",
        "thc": 20,
        "sativa": 60,
        "indika": 40,
        "harvestRate": 300,
        "height": 80,
        "harvestTime": 9
    },
    "bananaSplit": {
        "name": "Banana Split",
        "thc": 15,
        "sativa": 60,
        "indika": 40,
        "harvestRate": 500,
        "height": 100,
        "harvestTime": 9
    },
    "gorillaGlue": {
        "name": "Gorilla Glue",
        "thc": 30,
        "sativa": 50,
        "indika": 50,
        "harvestRate": 400,
        "height": 100,
        "harvestTime": 9
    },
    "purpleHaze": {
        "name": "Purple Haze",
        "thc": 21,
        "sativa": 70,
        "indika": 30,
        "harvestRate": 500,
        "height": 120,
        "harvestTime": 10
    },
    "pineappleExpress": {
        "name": "Pineapple Express",
        "thc": 20,
        "sativa": 70,
        "indika": 30,
        "harvestRate": 600,
        "height": 140,
        "harvestTime": 9
    },
    "afghanKush": {
        "name": "Afghan Kush",
        "thc": 21.60,
        "sativa": 0,
        "indika": 100,
        "harvestRate": 600,
        "height": 150,
        "harvestTime": 8
    },
    "greenCrack": {
        "name": "Green Crack",
        "thc": 20,
        "sativa": 60,
        "indika": 40,
        "harvestRate": 650,
        "height": 90,
        "harvestTime": 8
    },
    "fatBanana": {
        "name": "Fat Banana",
        "thc": 25,
        "sativa": 30,
        "indika": 70,
        "harvestRate": 500,
        "height": 200,
        "harvestTime": 8
    },
    "oGKush": {
        "name": "OG Kush",
        "thc": 19,
        "sativa": 25,
        "indika": 75,
        "harvestRate": 475,
        "height": 220,
        "harvestTime": 9
    },
    "crystalMeth": {
        "name": "OG Kush",
        "thc": 20,
        "sativa": 60,
        "indika": 40,
        "harvestRate": 600,
        "height": 120,
        "harvestTime": 9
    },
    "greenGelato": {
        "name": "Green Gelato",
        "thc": 27,
        "sativa": 45,
        "indika": 55,
        "harvestRate": 800,
        "height": 200,
        "harvestTime": 10
    },
    "whiteWidow": {
        "name": "White Widow",
        "thc": 19,
        "sativa": 50,
        "indika": 50,
        "harvestRate": 600,
        "height": 190,
        "harvestTime": 9
    },
    "aK47": {
        "name": "AK 47",
        "thc": 20,
        "sativa": 80,
        "indika": 20,
        "harvestRate": 500,
        "height": 130,
        "harvestTime": 9
    },
    "northernLight": {
        "name": "Northern Light",
        "thc": 19,
        "sativa": 40,
        "indika": 60,
        "harvestRate": 1200,
        "height": 300,
        "harvestTime": 7
    },
    "cookiesGirlScout": {
        "name": "Cookies Girl Scout",
        "thc": 22,
        "sativa": 60,
        "indika": 40,
        "harvestRate": 300,
        "height": 100,
        "harvestTime": 8
    },
    "bruceBanner": {
        "name": "Bruce Banner",
        "thc": 25,
        "sativa": 65,
        "indika": 35,
        "harvestRate": 1000,
        "height": 280,
        "harvestTime": 10
    },
    "californiaKush": {
        "name": "California Kush",
        "thc": 20,
        "sativa": 35,
        "indika": 65,
        "harvestRate": 800,
        "height": 550,
        "harvestTime": 9
    },
    "excalibur": {
        "name": "Excalibur",
        "thc": 19,
        "sativa": 80,
        "indika": 20,
        "harvestRate": 600,
        "height": 300,
        "harvestTime": 9
    },
    "sourDiesel": {
        "name": "Sour Diesel",
        "thc": 18.15,
        "sativa": 30,
        "indika": 70,
        "harvestRate": 700,
        "height": 110,
        "harvestTime": 9
    },
    "whiteSkunk": {
        "name": "White Skunk",
        "thc": 15,
        "sativa": 70,
        "indika": 30,
        "harvestRate": 800,
        "height": 80,
        "harvestTime": 11        
    },
    "lemonSkunk": {
        "name": "Lemon Skunk",
        "thc": 19.12,
        "sativa": 50,
        "indika": 50,
        "harvestRate": 800,
        "height": 95,
        "harvestTime": 8
    },
    "superLemonHaze": {
        "name": "Super Lemon Haze",
        "thc": 19.33,
        "sativa": 70,
        "indika": 30,
        "harvestRate": 800,
        "height": 140,
        "harvestTime": 10
    },
    "superSilverHaze": {
        "name": "Super Silver Haze",
        "thc": 19.11,
        "sativa": 70,
        "indika": 30,
        "harvestRate": 1500,
        "height": 180,
        "harvestTime": 11
    },
    "whiteRhino": {
        "name": "White Rhino",
        "thc": 20.19,
        "sativa": 20,
        "indika": 80,
        "harvestRate": 900,
        "height": 160,
        "harvestTime": 9
    },
    "orientExpress": {
        "name": "Orient Express",
        "thc": 16,
        "sativa": 60,
        "indika": 40,
        "harvestRate": 650,
        "height": 130,
        "harvestTime": 10
    },
    "critical": {
        "name": "Critical",
        "thc": 18,
        "sativa": 25,
        "indika": 85,
        "harvestRate": 600,
        "height": 200,
        "harvestTime": 8
    },
    "royaleHaze": {
        "name": "Royale Haze",
        "thc": 20,
        "sativa": 85,
        "indika": 15,
        "harvestRate": 500,
        "height": 450,
        "harvestTime": 12
    },
    "santaSativa": {
        "name": "Santa Sativa",
        "thc": 20,
        "sativa": 70,
        "indika": 30,
        "harvestRate": 500,
        "height": 450,
        "harvestTime": 12
    },
    "kaliAK": {
        "name": "Kali AK",
        "thc": 20,
        "sativa": 75,
        "indika": 25,
        "harvestRate": 480,
        "height": 160,
        "harvestTime": 9
    }
}