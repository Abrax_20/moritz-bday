'use strict';

import fs from 'fs';
import path from 'path';
import cors from 'cors';
import http from 'http';
import express from 'express';
import SocketIO from 'socket.io';
import { v4 as uuidv4 } from 'uuid';

import { CARDS } from './cards';
import { getPlayerCards, checkMatchWinner, changeCardOrder, pushWinner } from './util';
import { resolve } from 'path';
import { rejects } from 'assert';

let app = express();
let server = http.Server(app);
let io = new SocketIO(server);
let port = process.env.PORT || 3000;

app.use(express.json());
app.use(cors({ credentials: true, optionsSuccessStatus: 200 }));

const gameInstances = {};


app.get('/', (req, res) => {
  res.json(CARDS);
});

app.get('/:name', (req, res) => {
  if (!req.params.name || !CARDS[req.params.name]) {
    res.status(404).json({ error: true });
    return;
  }

  res.json(CARDS[req.params.name]);
})

app.get('/:name/image', (req, res) => {
  if (!req.params.name || !CARDS[req.params.name]) {
    res.status(404).json({ error: true });
    return;
  }
  const fileStream = fs.createReadStream(path.resolve(__dirname, `../images/${req.params.name}.jpg`));
  fileStream.on('open', function () {
      res.set('Content-Type', 'image/jpg');
      fileStream.pipe(res);
  });
  fileStream.on('error', function (err) {
      res.status(404).json({ error: true });
  });
})

app.post('/game', (req, res) => {
  const roomId = uuidv4();
  gameInstances[roomId] = {}
  res.json({ roomId });
});

function getClientsInRoom(roomId) {
  return new Promise((resolve, rejects) => {
    io.sockets.in(roomId).clients((err, clients) => {
      if (err) {
        rejects(err);
        return;
      }

      resolve(clients);
    })
  })
}

io.on('connection', (socket) => {
  console.log("Connection");
  socket.on('START_GAME', () => {});

  socket.on('JOIN_GAME', async (roomId) => {
    try {
      io.to(roomId).emit('USER_JOINED');
      socket.join(roomId);
      const players = await getClientsInRoom(roomId);
  
      if (players.length == 2) {
        const [player1Cards, player2Cards] = getPlayerCards();
        gameInstances[roomId] = {
          activePlayer: 'player1',
          player1: { cards: player1Cards },
          player2: { cards: player2Cards },
        }

        io.to(players[0]).emit('START_GAME', { isActive: true, activeCard: player1Cards[0], numberOfCards: player1Cards.length });
        io.to(players[1]).emit('START_GAME', { isActive: false, activeCard: player2Cards[0], numberOfCards: player2Cards.length });
      }
    } catch (err) {
      console.log(err);
    }
  });

  socket.on('RESTART_GAME', async (roomId) => {
    try {
      const players = await getClientsInRoom(roomId);
      const [player1Cards, player2Cards] = getPlayerCards();
      gameInstances[roomId] = {
        activePlayer: 'player1',
        player1: { cards: player1Cards },
        player2: { cards: player2Cards },
      }

      io.to(players[0]).emit('START_GAME', { isActive: true, activeCard: player1Cards[0], numberOfCards: player1Cards.length });
      io.to(players[1]).emit('START_GAME', { isActive: false, activeCard: player2Cards[0], numberOfCards: player2Cards.length });
    } catch (err) {
      console.log(err);
    }
  })

  socket.on('ROUND', async (roomId, key) => {
    try {
      const game = gameInstances[roomId];
      const players = await getClientsInRoom(roomId);
      const isPlayer1Active = game.activePlayer === 'player1';
      game.activePlayer = isPlayer1Active ? 'player2' : 'player1';
      const winner = checkMatchWinner(key, CARDS[game.player1.cards[0]], CARDS[game.player2.cards[0]]);
  
      if (winner === -1) {
        game.player1.cards = changeCardOrder(game.player1.cards);
        game.player2.cards = changeCardOrder(game.player2.cards);
        console.log('no winner');
        io.to(players[0]).emit('FINISH_ROUND', { isActive: !isPlayer1Active, hatWon: false, activeCard: game.player1.cards[0], numberOfCards: game.player1.cards.length });
        io.to(players[1]).emit('FINISH_ROUND', { isActive: isPlayer1Active, hatWon: false, activeCard: game.player2.cards[0], numberOfCards: game.player2.cards.length });
        return;
      }

      if (winner === 0) {
        console.log('player one wins');
        const [player1Cards, player2Cards] = pushWinner(game.player1.cards, game.player2.cards)
        game.player1.cards = player1Cards;
        game.player2.cards = player2Cards;

        if (game.player1.cards.length > 0 && game.player2.cards.length > 0) {
          io.to(players[0]).emit('FINISH_ROUND', { isActive: !isPlayer1Active, hatWon: true, activeCard: game.player1.cards[0], numberOfCards: game.player1.cards.length });
          io.to(players[1]).emit('FINISH_ROUND', { isActive: isPlayer1Active, hatWon: false, activeCard: game.player2.cards[0], numberOfCards: game.player2.cards.length });
          return;
        }
      } else {
        console.log('player two wins');
        const [player2Cards, player1Cards] = pushWinner(game.player2.cards,game.player1.cards)
        game.player1.cards = player1Cards;
        game.player2.cards = player2Cards;
        if (game.player1.cards.length > 0 && game.player2.cards.length > 0) {
          io.to(players[0]).emit('FINISH_ROUND', { isActive: !isPlayer1Active, hatWon: false, activeCard: game.player1.cards[0], numberOfCards: game.player1.cards.length });
          io.to(players[1]).emit('FINISH_ROUND', { isActive: isPlayer1Active, hatWon: true, activeCard: game.player2.cards[0], numberOfCards: game.player2.cards.length });
          return;
        }
      }
    
      if (game.player1.cards.length === 0) {
        delete gameInstances[roomId];
        io.to(players[0]).emit('LOSE_GAME');
        io.to(players[1]).emit('WON_GAME');
      }
    
      if (game.player2.cards.length === 0) {
        delete gameInstances[roomId];
        io.to(players[0]).emit('WON_GAME');
        io.to(players[1]).emit('LOSE_GAME');
      }  
    } catch (err) {
      console.log(err);
    }
  })
});

server.listen(port, () => {
  console.log('[INFO] Listening on *:' + port);
});