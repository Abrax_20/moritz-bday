import { all, select, takeEvery } from 'redux-saga/effects';

import { socket } from '../App/socket';
import { JOIN_GAME, ROUND, RESTART_GAME } from '../actions/socket';

function* setResultSaga(action: any) {
    const { game } = yield select();
    socket.emit('ROUND', game.id, action.payload.key);
}

function* joinGameSaga(action: any) {
    socket.emit('JOIN_GAME', action.payload.roomId);
}

function* restartGameSaga(action: any) {
    const { game } = yield select();
    socket.emit('RESTART_GAME', game.id);
}


export function* rootSocketSaga() {
    yield all([
        takeEvery(RESTART_GAME, restartGameSaga),
        takeEvery(JOIN_GAME, joinGameSaga),
        takeEvery(ROUND, setResultSaga)
    ]);
}