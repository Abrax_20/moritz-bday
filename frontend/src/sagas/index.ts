import { rootSocketSaga } from './socket';

export function runSagas(sagaMiddleware: any) {
    sagaMiddleware.run(rootSocketSaga);
}