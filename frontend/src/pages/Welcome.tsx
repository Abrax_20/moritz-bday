import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import Clipboard from 'react-clipboard.js';

import { getClient } from '../helper';
import { StateType } from '../reducers';

const StyledContainer = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
`;
const StyledInfoText = styled.p``;
const StyledLink = styled.a``;
const StyledLinkButton = styled(Clipboard)`
    margin-top: 16px;
    width: 100px;
    height: 40px;
`;

export function Welcome() {
    const roomId = useSelector((state: StateType) => state.game.id);
    const link = `${getClient()}/${roomId}`;

    return (
        <StyledContainer>
            <StyledInfoText>Welcome moritz please share this link and send is to a friend.</StyledInfoText>
            <StyledLink href={link}>{link}</StyledLink>
            <StyledLinkButton data-clipboard-text={link}>
                Copy Link
            </StyledLinkButton>
        </StyledContainer>
    );
}
/* 

</StyledContainer><StyledLinkButton className="btn" data-clipboard-target="#foo">Copy Link</StyledLinkButton>
*/ 