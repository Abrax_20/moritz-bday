import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';

import { fetchCardData } from '../api';
import { Card } from '../components/Card';
import { StateType } from '../reducers';

import { restartGame } from '../actions/socket';

const StyledContainer = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
`;
const StyledHeader = styled.h1`
    font-size: 24px;
`;
const StyledGameEndHeader = styled.h1`
    font-size: 32px;
    text-align: center;
`;
const StyledGameEndCard = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    border: 1px solid #000;
    justify-content: center;
`;
const StyledRetryButton = styled.button`
    border: none;
    color: #000;
    padding: 16px;
    outline: none;
    font-size: 22px;
    cursor: pointer;
    margin: 16px 0 0;
    font-weight: 800;
    border-radius: 5px;
    text-decoration: none;
    background-color: #ff9900;
`;

export function Game() {
    const dispatch = useDispatch();
    const [cardInfo, setCardInfo] = useState<any>();
    const card = useSelector((state: StateType) => state.game.card);
    const numberOfCards = useSelector((state: StateType) => state.game.numberOfCards);
    const isActive = useSelector((state: StateType) => state.game.isActivePlayer);
    const isWinner = useSelector((state: StateType) => state.game.isWinner);


    useEffect(() => {
        if (card) {
            fetchCardData(card).then((data) => setCardInfo(data));
        }
    }, [card])

    if (isWinner !== null) {
        return (
            <StyledContainer>
                <StyledGameEndCard>
                    <StyledGameEndHeader>{isWinner ? 'You have won :)' : 'You have lost :('}</StyledGameEndHeader>
                    <div style={{ flex: 1 }} />
                    <StyledRetryButton onClick={() => dispatch(restartGame())}>Retry</StyledRetryButton>
                </StyledGameEndCard>
            </StyledContainer>
        );
    } 

    if (!card || !cardInfo) return null;

    return (
        <StyledContainer>
            <StyledHeader>Cards: {numberOfCards}</StyledHeader>
            <Card id={card} isActive={isActive} weed={cardInfo} />
        </StyledContainer>
    );
}