import React from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { API_URL } from '../api';
import { round } from '../actions/socket';

const StyledContainer = styled.div<{ isActive: boolean; }>`
    flex: 1;
    display: flex;
    flex-direction: column;
    border: 1px solid #000;
    background-color: ${({ isActive }) => isActive ? 'transparent' : 'rgba(200, 200, 200, 0.5)'};
`;
const StyledHeadline = styled.h1`
    margin: 8px 0;
    text-align: center;
`;
;const StyledHeaderImage = styled.img`
    flex: 1;
    max-height: 250px;
    object-fit: contain;
`;
const StyledRow = styled.button`
    flex: 1;
    border: none;
    outline: none;
    display: flex;
    cursor: pointer;
    padding: 0 16px;
    align-items: center;
    border-top: 1px solid black;
    background-color: transparent;

    &:hover {
        background-color: rgba(200, 200, 200, 0.3);
    }
`;
const StyledMenu = styled.div<{ isActive: boolean; }>`
    flex: 1;
    display: flex;
    flex-direction: column;
    background-color: ${({ isActive }) => isActive ? 'transparent' : 'rgba(200, 200, 200, 0.3)'};
    ${StyledRow} {
        background-color: ${({ isActive }) => isActive ? 'transparent' : 'rgba(200, 200, 200, 0.3)'};
    }
`;
const StyledRowTitle = styled.p`
    flex: 1;
    margin: 0;
    font-size: 24px;
    text-align: left;
    
`;
const StyledRowValue = styled.p`
    margin: 0;
    font-size: 24px;
    font-weight: 600;
`;

type PropsType = {
    id: string;
    isActive: boolean;
    weed: {
        thc: number;
        name: string;
        height: number;
        sativa: number;
        indika: number;
        harvestTime: number;
        harvestRate: number;
    }
}
export function Card({ id, isActive, weed: { thc, name, height, sativa, indika, harvestTime, harvestRate } }: PropsType) {
    const dispatch = useDispatch();

    return (
        <StyledContainer isActive={isActive}>
            <StyledHeadline>{name}</StyledHeadline>
            <StyledHeaderImage src={`${API_URL}/${id}/image`} />
            <StyledMenu isActive={isActive}>
                <StyledRow onClick={() => isActive && dispatch(round('thc'))}>
                    <StyledRowTitle>THC Gehalt:</StyledRowTitle>
                    <StyledRowValue>{thc}%</StyledRowValue>
                </StyledRow>
                <StyledRow onClick={() => isActive && dispatch(round('indika'))}>
                    <StyledRowTitle>Indika Anteil:</StyledRowTitle>
                    <StyledRowValue>{indika}%</StyledRowValue>
                </StyledRow>
                <StyledRow onClick={() => isActive && dispatch(round('sativa'))}>
                    <StyledRowTitle>Sativa Anteil:</StyledRowTitle>
                    <StyledRowValue>{sativa}%</StyledRowValue>
                </StyledRow>
                <StyledRow onClick={() => isActive && dispatch(round('height'))}>
                    <StyledRowTitle>Außenhöhe:</StyledRowTitle>
                    <StyledRowValue>{height}cm</StyledRowValue>
                </StyledRow>
                <StyledRow onClick={() => isActive && dispatch(round('harvestRate'))}>
                    <StyledRowTitle>Ernte pro m2:</StyledRowTitle>
                    <StyledRowValue>{harvestRate}g</StyledRowValue>
                </StyledRow>
                <StyledRow onClick={() => isActive && dispatch(round('harvestTime'))}>
                    <StyledRowTitle>Blütezeit:</StyledRowTitle>
                    <StyledRowValue>{harvestTime} Wochen</StyledRowValue>
                </StyledRow>
            </StyledMenu>
        </StyledContainer>
    )
}