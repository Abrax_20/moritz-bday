import { JOIN_GAME, START_GAME, FINISH_ROUND, WIN_GAME, LOSE_GAME } from "../actions/socket";

export type GameStateType = {
    id?: string;
    card?: string;
    isPlaying: boolean;
    numberOfCards: number;
    isActivePlayer: boolean;
    isWinner: boolean | null;
    hatWonLastRound?: boolean;
};
export const initalState: GameStateType = {
    isWinner: null,
    numberOfCards: 0,
    isPlaying: false,
    isActivePlayer: false,
};
export function gameReducer(state = initalState, action: any): GameStateType {
    switch(action.type) {
        case WIN_GAME: {
            return {
                ...state,
                isWinner: true,
            }
        }
        case LOSE_GAME: {
            return {
                ...state,
                isWinner: false,
            }
        }
        case JOIN_GAME: {
            return {
                ...state,
                id: action.payload.roomId
            };
        }
        case FINISH_ROUND: {
            return {
                ...state,
                card: action.payload.activeCard,
                hatWonLastRound: action.payload.hatWon, 
                isActivePlayer: action.payload.isActive,
                numberOfCards: action.payload.numberOfCards
            }
        }
        case START_GAME: {
            return {
                ...state,
                isWinner: null,
                isPlaying: true,
                card: action.payload.activeCard,
                isActivePlayer: action.payload.isActive,
                numberOfCards: action.payload.numberOfCards
            }
        }
        default: {
            return state;
        }
    }
}
export default gameReducer;