import { combineReducers } from 'redux';

import game, { GameStateType } from './game'

export type StateType = {
    game: GameStateType;
}
export const reducers = combineReducers({
    game
});
export default reducers;