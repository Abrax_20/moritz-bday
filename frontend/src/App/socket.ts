import io from 'socket.io-client';

import { startGame, finishRound, loseGame, winGame } from '../actions/socket';

export const socket = io('ws://localhost:3000');

export function initSocket({ dispatch }: any) {
    socket.on('connect', () => {
        console.log('Socket connected');
    });

    socket.on('START_GAME', ({ isActive, activeCard, numberOfCards }: any) => {
        dispatch(startGame(isActive, activeCard, numberOfCards));
    });

    socket.on('FINISH_ROUND', ({ isActive , hatWon, activeCard, numberOfCards }: any) => {
        dispatch(finishRound(isActive , hatWon, activeCard, numberOfCards))
    })

    socket.on('WON_GAME', () => {
        dispatch(winGame());
    })

    socket.on('LOSE_GAME', () => {
        dispatch(loseGame());
    })

    socket.on('message', (data: any) => {
        console.log('WELCOME');
        console.log(data);
    });
}
