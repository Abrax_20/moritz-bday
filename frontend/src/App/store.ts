import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';

import reducers from '../reducers';
import { runSagas } from '../sagas';
import { initSocket } from './socket';

const logger = createLogger({});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  reducers,
  applyMiddleware(...[logger, sagaMiddleware])
);
runSagas(sagaMiddleware);
initSocket(store);

export { store };