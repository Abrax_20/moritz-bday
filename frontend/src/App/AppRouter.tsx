import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { joinGame } from '../actions/socket';

import { getClient } from '../helper';
import { fetchGameRoomId } from '../api';
import { StateType } from '../reducers';

import { Game } from '../pages/Game'; 
import { Welcome } from '../pages/Welcome';

export function AppRouter() {
    const dispatch = useDispatch();
    const game = useSelector((state: StateType) => state.game); 
    const [roomId, setRoomId] = useState(window.location.href.replace(`${getClient()}`, '').replace(/\//g, ''));
  
    useEffect(() => {
      if (!roomId) {
        fetchGameRoomId().then((roomId: any) => {
          setRoomId(roomId);
          window.location.replace(`${getClient()}/${roomId}`)
        });
      }
    }, []);
  
    useEffect(() => {
      dispatch(joinGame(roomId));
    }, [roomId]);

    if (game.isPlaying) {
        return <Game />;
    }

    return <Welcome />;
}