export const START_GAME = 'START_GAME';
export function startGame(isActive: boolean, activeCard: any, numberOfCards: number) {
    return { type: START_GAME, payload: { isActive, activeCard, numberOfCards } };
}

export const RESTART_GAME = 'RESTART_GAME';
export function restartGame() {
    return { type: RESTART_GAME, payload: {} };
}

export const ROUND = 'ROUND';
export function round(key: 'thc' | 'sativa' | 'indika' | 'height' | 'harvestRate' | 'harvestTime') {
    return { type: ROUND, payload: { key } };
}

export const FINISH_ROUND = 'FINISH_ROUND';
export function finishRound(isActive: boolean, hatWon: boolean, activeCard: string, numberOfCards: number) {
    return { type: FINISH_ROUND, payload: { isActive , hatWon, activeCard, numberOfCards } };
}

export const JOIN_GAME = 'JOIN_GAME';
export function joinGame(roomId: string) {
    return { type: JOIN_GAME, payload: { roomId } };
}

export const WIN_GAME = 'WIN_GAME';
export function winGame() {
    return { type: WIN_GAME, payload: {} };
}

export const LOSE_GAME = 'LOSE_GAME';
export function loseGame() {
    return { type: LOSE_GAME, payload: {} };
}