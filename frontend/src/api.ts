export const API_URL = 'http://localhost:3000';

export async function fetchGameRoomId() {
    const response = await fetch(`${API_URL}/game`, {
        method: 'POST'
    });
    const { roomId } = await response.json();
    return roomId;
}

export async function fetchCardData(name: string) {
    const response = await fetch(`${API_URL}/${name}`)
    return response.json();
}